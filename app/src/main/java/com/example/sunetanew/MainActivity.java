package com.example.sunetanew;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        HomeFragment.OnHomeFragmentTransactionListener,
        OnlineAppointmentRequestFragment.OnSecondFragmentTransactionListener{

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  hideNavigationBar() ;

        //////////////////////////////////////////for oreo and higher///////////////////////////////////////////
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout=findViewById(R.id.drawer);
        navigationView=findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle =new ActionBarDrawerToggle(this,drawerLayout,toolbar, R.string.open,R.string.close);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.black));
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //load default fragment
////////////////////////////////////////////////////////////////
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fm_container,new HomeFragment());
        fragmentTransaction.commit();
////////////////////////////////////////////////////////////////////////

    }

    private void hideNavigationBar() {
        {

            this.getWindow().getDecorView()
                    .setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    );
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        drawerLayout.closeDrawer(GravityCompat.START);

        if(item.getItemId()==R.id.homeId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new HomeFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.onlineappointmentId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new OnlineAppointmentRequestFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.appointmenthistoryId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new AppointmentHistoryFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.finddoctorId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new FindDoctorFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.trackprogressId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new TrackProgressFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.eyetestId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new EyeTestFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.faqsId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new FaqsFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.newseventsId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new NewsEventsFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.testimonialsId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new TestimonialsFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.rule20Id){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new RuleFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.setreminder){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new SetReminderFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.contactsunetraId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new ContactSunetraFragment());
            fragmentTransaction.commit();
        }
        else if(item.getItemId()==R.id.bluelightfilterId){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fm_container,new BlueLightFilterFragment());
            fragmentTransaction.commit();
        }

        return true;
    }
    ////////////////////////////////////////////////close window////////////////////////////////////////////
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onBackPressed() {
//        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
//            getSupportFragmentManager().popBackStackImmediate();
//            getSupportFragmentManager().beginTransaction().commit();
//        } else {
//            appCloseDialog("Do you want to close the app ?", this);
//        }
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new HomeFragment(), "HomeFragment")
                .addToBackStack("HomeFragmnt")
                .commit();

    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onHomeFragmentSuccessFragmentListener() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new OnlineAppointmentRequestFragment(), "SecondFragment")
                .addToBackStack("SecondFragment")
                .commit();
    }

    @Override
    public void onSecondFragmentTransactionListener() {

    }
    public void appCloseDialog(String msg, AppCompatActivity ctx) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(startMain);
                        dialog.dismiss();
                        ctx.finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


}