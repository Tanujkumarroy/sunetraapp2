
package com.example.sunetanew.ModelClass.DoctorList;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorList {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private ArrayList<$value> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public ArrayList<$value> get$values() {
        return $values;
    }

    public void set$values(ArrayList<$value> $values) {
        this.$values = $values;
    }

}
