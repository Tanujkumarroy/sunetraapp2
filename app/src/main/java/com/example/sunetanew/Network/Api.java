package com.example.sunetanew.Network;

import com.example.sunetanew.ModelClass.DepartmentList;
import com.example.sunetanew.ModelClass.DoctorList.DoctorList;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {

    @GET("/api/webapi/DepartmentList")
    Call<DepartmentList>DEPARTMENT_LIST_CALL();

    @GET("/api/webapi/DoctorList")
    Call<DoctorList>DOCTOR_LIST_CALL();
}
