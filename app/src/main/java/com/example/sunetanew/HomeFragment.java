package com.example.sunetanew;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sunetanew.Adapter.GridDesignAdapter;

public class HomeFragment extends BaseFragment implements
        View.OnClickListener {

    private OnHomeFragmentTransactionListener mListener;
    private boolean resume;
    private View view;
    private RecyclerView rv_travel_more;
    private RecyclerView.Adapter travelAndMoreAdapter;
    private RecyclerView.LayoutManager travelAndMorListLayoutManager;
    private LinearLayout ll_main;
    private AppCompatTextView atv_click_here;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = onCreateView(inflater, container, savedInstanceState, R.layout.fragment_main);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
        }
        resume = true;
    }

    @SuppressLint("WrongConstant")
    private void setUI(View view) {

        atv_click_here = view.findViewById(R.id.atv_click_here);
        atv_click_here.setOnClickListener(this);

        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
        rv_travel_more = view.findViewById(R.id.rv_travel_more);
        travelAndMorListLayoutManager = new GridLayoutManager(getActivity(), 3);
        rv_travel_more.setLayoutManager(travelAndMorListLayoutManager);
        rv_travel_more.setNestedScrollingEnabled(false);
        rv_travel_more.setHasFixedSize(true);

        travelAndMoreAdapter = new GridDesignAdapter(getActivity());
        rv_travel_more.setAdapter(travelAndMoreAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeFragmentTransactionListener) {
            mListener = (OnHomeFragmentTransactionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.atv_click_here:
                mListener.onHomeFragmentSuccessFragmentListener();
                break;
        }
    }

    public interface OnHomeFragmentTransactionListener {
        void onHomeFragmentSuccessFragmentListener();
    }
}
